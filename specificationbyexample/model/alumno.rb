require_relative 'materia'

class Alumno
  def initialize(nombre)
    @nombre = nombre
    # Key=nombre de la materia, Value=materia
    @materias = {}
  end

  def inscribir_en(materia)
    @materias[materia.nombre()] = materia
  end

  def asignar_tarea(materia, tarea)
    return unless @materias.key?(materia.nombre())
    @materias[materia.nombre()].agregar_tarea(tarea)
  end

  def debe_realizar_tareas?(tareas)
    for materia in @materias
      return false unless materia.posee_tareas?(tareas)
    end
    return true
  end

  def asignar_proyecto(materia, proyecto)
    return unless @materias.key?(materia.nombre())
    @materias[materia.nombre()].agregar_proyecto(proyecto)
  end

  def debe_realizar_proyecto?(proyecto)
    for materia in @materias
      return false unless materia.posee_proyecto?(proyecto)
    end
    return true
  end

  def asignar_condicion(cond)
    @condicion = cond
  end

  def condicion?(cond)
    @condicion == cond
  end

  def calificar_tarea(materia, tarea, nota)
    return unless @materias.key?(materia.nombre)
    @materias[materia.nombre].calificar_tarea(tarea, nota)
  end

  def cargar_asistencia(materia, porcentaje_asist)
    return unless @materias.key?(materia.nombre)
    @materias[materia.nombre].cargar_asistencia(porcentaje_asist)
  end

  def aprobo_iteracion_proyecto?(materia, iter)
    return unless @materias.key?(materia.nombre)
    @materias[materia.nombre].agregar_iteracion_proyecto_aprobada(iter)
  end

  def aprobo_materia?(materia)
    return unless @materias.key?(materia.nombre)
    @materias[materia.nombre].aprobo?
  end


end
