class Materia
  attr_reader :nombre

  def initialize(nombre)
    @nombre = nombre
    # Key=nombre de la tarea, Value=tarea
    @tareas = {}
  end

  def agregar_tarea(tarea)
    @tareas[tarea.nombre()] = tarea
  end

  def posee_tareas?(tareas)
    for tarea in tareas
      return false unless @tareas.key?(tarea)
    end
    return true
  end

  def agregar_proyecto(proyecto)
    @proyecto = proyecto
  end

  def posee_proyecto?(proyecto)
    @proyecto.nombre?(proyecto)
  end

  def calificar_tarea(tarea, nota)
    return unless @tareas.key?(tarea.nombre)
    @tareas[tarea.nombre].calificar(nota)
  end

  def cargar_asistencia(porcentaje_asist)
    @porcentaje_asist = porcentaje_asist
  end

  def agregar_iteracion_proyecto_aprobada(iter)
    @proyecto.agregar_iteracion_aprobada(iter)
  end

  def aprobo?()
    aprobo_materias?() && (@porcentaje_asist >= 75) && @proyecto.aprobado?()
  end

  private

  def aprobo_materias?()
    for tarea in @tareas.values
      return false unless tarea.aprobada?()
    end
    return true
  end

end
