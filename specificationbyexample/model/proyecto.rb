class Proyecto
  def initialize(nombre)
    @nombre = nombre
    @iter_aprobadas = []
  end

  def nombre?(nombre)
    @nombre == nombre
  end

  def agregar_iteracion_aprobada(iter)
    @iter_aprobadas.push(iter)
  end

  def aprobado?()
    @iter_aprobadas.length >= 3
  end

end
