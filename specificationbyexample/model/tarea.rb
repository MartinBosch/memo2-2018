class Tarea
  attr_reader :nombre
  def initialize(nombre)
    @nombre = nombre
  end

  def ==(other)
    self.nombre == other.nombre
  end

  def calificar(nota)
    if nota >= 0
      @nota = nota
    end
  end

  def aprobada?()
    @nota >= 4 unless @nota == nil
  end

end
