Given(/^el alumno Juan$/) do
  @alumno = Alumno.new('Juan')
end

Given(/^la materia m(\d+)$/) do |arg1|
  @materia = Materia.new('m1')
  @alumno.inscribir_en(@materia)
end

Given(/^la tarea individual ta$/) do
  @ta = Tarea.new('ta')
  @alumno.asignar_tarea(@materia, @ta)
end

Given(/^la tarea individual tb$/) do
  @tb = Tarea.new('tb')
  @alumno.asignar_tarea(@materia, @tb)
end

Given(/^la tarea individual tc$/) do
  @tc = Tarea.new('tc')
  @alumno.asignar_tarea(@materia, @tc)
end

Given(/^el proyecto grupal pg$/) do
  @alumno.asignar_proyecto(@materia, Proyecto.new('pg'))
end

Given(/^el alumno regular Juan$/) do
  @alumno.asignar_condicion('regular')
end

When(/^aprobo todas las tareas semanales$/) do
  @alumno.calificar_tarea(@materia, @ta, 4)
  @alumno.calificar_tarea(@materia, @tb, 6)
  @alumno.calificar_tarea(@materia, @tc, 9)
end

When(/^asistio al (\d+)% de las clases$/) do |arg1|
  @alumno.cargar_asistencia(@materia, Integer(arg1))
end

When(/^aprobo al menos (\d+) iterationes del proyecto pg$/) do |arg1|
  for i in 1..Integer(arg1)
    @alumno.aprobo_iteracion_proyecto?(@materia, i)
  end
end

Then(/^aprobo la materia$/) do
  expect(@alumno.aprobo_materia?(@materia)).to be true
end

Then(/^No aprobo la materia$/) do
  expect(@alumno.aprobo_materia?(@materia)).to be false
end

When(/^no aprobo la tarea individual tc$/) do
  @alumno.calificar_tarea(@materia, @tc, 2)
end
