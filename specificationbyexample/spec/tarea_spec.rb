require 'rspec'
require_relative '../model/tarea'

describe 'Tarea' do

  let(:tarea) { Tarea.new('ta') }

  it 'Si una tarea se califico con 0 esta desaprobada' do
    tarea.calificar(0)
    expect(tarea.aprobada?()).to eq false
  end

  it 'Si una tarea se califico con 3 esta desaprobada' do
    tarea.calificar(3)
    expect(tarea.aprobada?()).to eq false
  end

  it 'Si una tarea se califico con 4 esta aprobada' do
    tarea.calificar(4)
    expect(tarea.aprobada?()).to eq true
  end

end
