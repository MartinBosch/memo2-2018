require 'rspec'
require_relative '../model/proyecto'

describe 'Proyecto' do

  let(:proyecto) { Proyecto.new('pg') }

  it 'Si un proyecto tiene al menos 3 iteraciones aprobadas esta aprobado' do
    proyecto.agregar_iteracion_aprobada(1)
    proyecto.agregar_iteracion_aprobada(2)
    proyecto.agregar_iteracion_aprobada(3)
    expect(proyecto.aprobado?()).to eq true
  end

  it 'Si un proyecto tiene 2 iteraciones aprobadas esta desaprobado' do
    proyecto.agregar_iteracion_aprobada(1)
    proyecto.agregar_iteracion_aprobada(2)
    expect(proyecto.aprobado?()).to eq false
  end

end
