require 'rspec'
require_relative '../model/alumno'
require_relative '../model/tarea'
require_relative '../model/proyecto'
require_relative '../model/materia'

describe 'Alumno' do

  let(:alumno) { Alumno.new('Juan') }

  it 'Al asignar una tarea al alumno juan esta queda bien asignada' do
    materia = Materia.new('m1')
    alumno.asignar_tarea(materia, Tarea.new('ta'))
    expect(alumno.debe_realizar_tareas?(['ta'])).to eq true
  end

  it 'Al asignar mas de una tarea al alumno juan estas quedan asignadas al mismo' do
    materia = Materia.new('m1')
    alumno.asignar_tarea(materia, Tarea.new('ta'))
    alumno.asignar_tarea(materia, Tarea.new('tb'))
    expect(alumno.debe_realizar_tareas?(['ta', 'tb'])).to eq true
  end

  it 'Al asignar un proyecto al alumno juan este se le asigno al mismo' do
    materia = Materia.new('m1')
    alumno.asignar_proyecto(materia, Proyecto.new('pg'))
    expect(alumno.debe_realizar_proyecto?('pg')).to eq true
  end

  it 'Si un alumno es regular su condicion debe indicar esto' do
    alumno.asignar_condicion('regular')
    expect(alumno.condicion?('regular')).to eq true
  end

  it 'Si un alumno es regular no puede ser condicional' do
    alumno.asignar_condicion('regular')
    expect(alumno.condicion?('condicional')).to eq false
  end

end
