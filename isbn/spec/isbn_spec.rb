require 'rspec'
require_relative '../model/isbn'

describe 'Isbn' do

  let(:isbn) { Isbn.new }

  it 'isbn vacio deberia ser invalido' do
    expect(isbn.is_valid("")).to eq false
  end

  it 'isbn de largo menor a 10 deberia ser invalido' do
    expect(isbn.is_valid("1 2")).to eq false
  end

  it 'isbn de largo menor a 10 deberia ser invalido' do
    expect(isbn.is_valid("123-44")).to eq false
  end

  it 'isbn de largo menor a 10 deberia ser invalido' do
    expect(isbn.is_valid("123456789")).to eq false
  end

  it 'isbn que contiene letras deberia ser invalido' do
    expect(isbn.is_valid("1234567a89")).to eq false
  end

  it 'isbn que contiene letras en cualquier pos deberia ser invalido' do
    expect(isbn.is_valid("1c34567889")).to eq false
  end

  it 'isbn que contiene letras en cualquier pos deberia ser invalido' do
    expect(isbn.is_valid("q134567889")).to eq false
  end

  it 'isbn que contiene letras en cualquier pos deberia ser invalido' do
    expect(isbn.is_valid("113456788Z")).to eq false
  end

  it 'isbn que contiene X en la ultima pos deberia ser valido' do
    expect(isbn.is_valid("155404295X")).to eq true
  end

  it 'isbn que contiene X en alguna pos que no sea la ultima deberia ser invalido' do
    expect(isbn.is_valid("155X042955")).to eq false
  end

  it 'isbn que contiene X en alguna pos que no sea la ultima deberia ser invalido' do
    expect(isbn.is_valid("X554042955")).to eq false
  end

  it 'isbn que contiene "-" deberia ser valido' do
    expect(isbn.is_valid("0-471-95869-7")).to eq true
  end

  it 'isbn que contiene " " deberia ser valido' do
    expect(isbn.is_valid("0 471 95869 7")).to eq true
  end

  it 'el check digit de 047195869 deberia ser 7' do
    expect(isbn.is_correct_check_digit("0471958697")).to eq true
  end

  it 'el check digit de 155404295 deberia ser X' do
    expect(isbn.is_correct_check_digit("155404295X")).to eq true
  end

  it 'el check digit de 0 471 95869 7 con separador deberia ser 7' do
    expect(isbn.is_valid("0471958697")).to eq true
  end

  it 'isbn de largo distinto de 10 deberia ser invalido' do
    expect(isbn.is_valid("15-5404-29511")).to eq false
  end

  it 'isbn de largo distinto de 10 deberia ser invalido' do
    expect(isbn.is_valid("15 5404 2")).to eq false
  end

end
