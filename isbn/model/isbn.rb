class Isbn

  def initialize()
    @sep = ["-", " "]
    @length = 10
    @check_digit = ["1","2","3","4","5","6","7","8","9","X"]
  end

  def is_i(num)
    num =~ /\A\d+\z/ ? true : false
  end

  #Determina si un digito de la primer parte del isbn
  #(sin contar el check digit) es validos
  def is_valid_digit(digit)
    @sep.include?(digit) || is_i(digit)
  end

  #Determina si todos los digitos de la primer parte del isbn
  #(sin contar el check digit) son validos
  def are_valid_firts_digits(num)
    for i in 0..num.length-2 do
      digit = num[i]
      if !is_valid_digit(digit)
        return false
      end
    end
    return true
  end

  def is_valid_check_digit(digit)
    is_i(digit) || digit == "X"
  end

  def get_check_digit(num)
    calculation = 0
    for i in 0..num.length-2 do
      digit = num[i].to_i
      calculation += digit * (i+1)
    end
    check = calculation % 11
    return @check_digit[check-1]
  end

  def is_correct_check_digit(num)
    get_check_digit(num) == num[@length-1]
  end

  def remove_separators(num)
    for s in @sep do
      num = num.tr(s, "")
    end
    return num
  end

	def is_valid(num)
    check_digit = num[num.length-1]
    num_only = remove_separators(num)

    return (num.length >= @length) &&
            are_valid_firts_digits(num) &&
            is_valid_check_digit(check_digit) &&
            is_correct_check_digit(num_only) &&
            (num_only.length == 10)
	end

end
