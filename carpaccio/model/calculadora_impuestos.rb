class CalculadoraImpuestos

  def initialize()
    @impuesto = { 'UT' => 0.0685, 'NV' => 0.08, 'TX' => 0.0625, 'AL' => 0.04, 'CA' => 0.0825 }
  end

  def aplicar_impuesto(total, prov)
    return total unless @impuesto.key?(prov)
    total * (1 + @impuesto[prov])
  end

end