require_relative 'calculadora_impuestos'
require_relative 'calculadora_descuentos_rangos'

class CalculadoraDescuentos

  def initialize()
    @calc_impuestos = CalculadoraImpuestos.new
    @calc_descuentos_rangos = CalculadoraDescuentosRangos.new
  end

  def calcular_total(cant, precio, prov)
    total = cant * precio
    total = @calc_descuentos_rangos.aplicar_descuento(total)
    total = @calc_impuestos.aplicar_impuesto(total, prov)
    total
  end
end