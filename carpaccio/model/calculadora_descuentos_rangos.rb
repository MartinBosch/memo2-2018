INF = 1.0/0.0

class CalculadoraDescuentosRangos

  def initialize()
    @rangos = []
    rango1 = 0..999
    rango2 = 1000..4999
    rango3 = 5000..6999
    rango4 = 7000..9999
    rango5 = 10000..49999
    rango6 = 50000..INF

    @rangos.push(rango1)
    @rangos.push(rango2)
    @rangos.push(rango3)
    @rangos.push(rango4)
    @rangos.push(rango5)
    @rangos.push(rango6)

    @descuento = { rango1 => 0.0, rango2 => 0.03, rango3 => 0.05, rango4 => 0.07,
                   rango5 => 0.1, rango6 => 0.15 }
  end

  def aplicar_descuento(total)
    rango = @rangos.detect { |r| r.include?(total) }
    total * (1 - @descuento[rango])
  end

end