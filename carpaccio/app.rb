require 'sinatra'
require 'json'
require_relative 'model/calculadora_descuentos'

get '/calcular_total' do
  cant = params['cant'].to_i
  precio = params['precio'].to_f
  prov = params['prov']

  calculadora = CalculadoraDescuentos.new
  result = calculadora.calcular_total(cant, precio, prov)

  content_type :json
  { :result => result}.to_json
end
