# spec/app_spec.rb

require File.expand_path '../spec_helper.rb', __FILE__

describe 'Sinatra Application' do

  it 'El total de 0 productos debe ser 0' do
    get '/calcular_total?cant=0&precio=0'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(0.0)
  end

  it 'El total de 1 producto de precio 2 debe ser 2' do
    get '/calcular_total?cant=1&precio=2'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(2.0)
  end

  it 'El total de 1 producto con cantidad 2 de precio 2 debe ser 4' do
    get '/calcular_total?cant=2&precio=2'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(4.0)
  end

  it 'El total de 500 con el impuesto de UT debe ser 534.25' do
    get '/calcular_total?cant=1&precio=500&prov=UT'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(534.25)
  end

  it 'El total de 1500 con el descuento y el impuesto de UT debe ser 1554.6675' do
    get '/calcular_total?cant=1&precio=1500&prov=UT'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(1554.6675)
  end

  it 'El total de 500 con el impuesto de NV debe ser 540' do
    get '/calcular_total?cant=1&precio=500&prov=NV'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(540.0)
  end

  it 'El total de 500 con el impuesto de TX debe ser 531.25' do
    get '/calcular_total?cant=1&precio=500&prov=TX'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(531.25)
  end

  it 'El total de 500 con el impuesto de AL debe ser 520' do
    get '/calcular_total?cant=1&precio=500&prov=AL'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(520.0)
  end

  it 'El total de 500 con el impuesto de CA debe ser 541.25' do
    get '/calcular_total?cant=1&precio=500&prov=CA'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(541.25)
  end

  it 'El total de 999 con el descuento y el impuesto de UT debe ser 1067.4315' do
    get '/calcular_total?cant=1&precio=999&prov=UT'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(1067.4315)
  end

  it 'El total de 1000 con el descuento y el impuesto de UT debe ser 1036.445' do
    get '/calcular_total?cant=1&precio=1000&prov=UT'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(1036.445)
  end

  it 'El total de 4999 con el descuento y el impuesto de UT debe ser 5181.188555' do
    get '/calcular_total?cant=1&precio=4999&prov=UT'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(5181.188555)
  end

  it 'El total de 5000 con el descuento y el impuesto de UT debe ser 5075.375' do
    get '/calcular_total?cant=1&precio=5000&prov=UT'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(5075.375)
  end

  it 'El total de 6999 con el descuento y el impuesto de UT debe ser 7104.509925' do
    get '/calcular_total?cant=1&precio=6999&prov=UT'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(7104.509924999999)
  end

  it 'El total de 7000 con el descuento y el impuesto de UT debe ser 6955.935' do
    get '/calcular_total?cant=1&precio=7000&prov=UT'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(6955.935)
  end

  it 'El total de 9999 con el descuento y el impuesto de UT debe ser 9936.056295' do
    get '/calcular_total?cant=1&precio=9999&prov=UT'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(9936.056295)
  end

  it 'El total de 10000 con el descuento y el impuesto de UT debe ser 1068.5' do
    get '/calcular_total?cant=1&precio=10000&prov=UT'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(9616.5)
  end

  it 'El total de 49999 con el descuento y el impuesto de UT debe ser 48081.53835' do
    get '/calcular_total?cant=1&precio=49999&prov=UT'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(48081.538349999995)
  end

  it 'El total de 50000 con el descuento y el impuesto de UT debe ser 45411.25' do
    get '/calcular_total?cant=1&precio=50000&prov=UT'
    data = JSON.parse(last_response.body)
    result = data['result']

    expect(last_response).to be_ok
    expect(result).to eq(45411.25)
  end

end
