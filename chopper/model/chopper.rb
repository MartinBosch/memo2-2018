class Chopper

# El chop devuelve la posición del elemento en el array
# El sum, devuelve el texto de los dígitos de la sumatoria de los números
# contenidos en el array siempre que los esa sumatoria no exceda los dos dígitos. 

	def chop(n, array)
		if array.empty?
			return -1
		end

		for i in 0..array.length
			if array[i] == n
				return i
			end
		end

		return -1
	end

	def num_to_text_indiv(num)
		case num
		when '0' 
			return 'cero'
		when '1' 
			return 'uno'
		when '2' 
			return 'dos'
		when '3' 
			return 'tres'
		when '4' 
			return 'cuatro'
		when '5' 
			return 'cinco'
		when '6' 
			return 'seis'
		when '7' 
			return 'siete'
		when '8' 
			return 'ocho'
		when '9' 
			return 'nueve'
		else
			return ''
		end
	end

	def num_to_text(num)
		text = Array.new
		num.to_s.split("").each{ |i| text.push( num_to_text_indiv(i) ) }
		return text.join(',')
	end

	def sum(array)
		if array.empty?
			return 'vacio'
		end
		sum = 0
		array.each{ |num| sum+=num }
		if sum > 99
			return 'demasiado grande'
		end

		return num_to_text(sum)
	end

end
