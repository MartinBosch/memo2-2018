require 'sinatra'
require 'json'
require_relative 'model/chopper'

def to_nums(string)
  nums_s = string.split(',')
  nums = []
  nums_s.each{ |n| nums.push(Integer(n)) }
  nums
end

get '/sum' do
  nums_s = params['x']
  nums = to_nums(nums_s)
  chopper = Chopper.new
  result = chopper.sum(nums)
  content_type :json
  { :saludo => "sum:#{nums_s}, resultado:#{result}"}.to_json
end

post '/chop' do
  num_s = params['x']
  num = Integer(num_s)
  nums_s = params['y']
  nums = to_nums(nums_s)
  chopper = Chopper.new
  indice = chopper.chop(num, nums)
  content_type :json
  { :saludo => "chop:x=#{num_s},y=#{nums_s}, resultado = #{indice}"}.to_json
end
