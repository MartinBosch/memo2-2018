class Chopper

# El chop devuelve la posición del elemento en el array
# El sum, devuelve el texto de los dígitos de la sumatoria de los números
# contenidos en el array siempre que los esa sumatoria no exceda los dos dígitos.

  def chop(n, array)
    if array.empty?
      return -1
    end

    for i in 0..array.length
      if array[i] == n
        return i
      end
    end

		return -1
	end

	def num_to_text_indiv(num)
		num_text = ['cero', 'uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis',
			         'siete', 'ocho', 'nueve']
		return num_text[num]
	end

	def num_separar_digitos(num)
		if num < 10
			return [num]
		else
			return [num/10, num%10]
		end
	end

	def num_to_text(num)
		text = Array.new
		num_sep = num_separar_digitos(num)
		num_sep.each{ |i| text.push(num_to_text_indiv(i)) }
		return text.join(',')
	end

	def sum(array)
		if array.empty?
			return 'vacio'
		end
		sum = 0
		array.each{ |num| sum += num }
		if sum > 99
			return 'demasiado grande'
		end

		return num_to_text(sum)
	end

end
