# spec/app_spec.rb

require File.expand_path '../spec_helper.rb', __FILE__

describe 'Sinatra Application' do

  it 'get /sum?x= should return vacio' do
    get '/sum?x='
    expect(last_response).to be_ok
    expect(last_response.body).to include 'sum:, resultado:vacio'
  end

  it 'get /sum?x=1 should return uno' do
    get '/sum?x=1'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'sum:1, resultado:uno'
  end

  it 'get /sum?x=9,9 should return uno' do
    get '/sum?x=9,9'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'sum:9,9, resultado:uno,ocho'
  end

  it 'get /sum?x=99,1 should return demasiado grande' do
    get '/sum?x=99,1'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'sum:99,1, resultado:demasiado grande'
  end

  it 'get /sum?x=98,1 should return demasiado grande' do
    get '/sum?x=98,1'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'sum:98,1, resultado:nueve,nueve'
  end

  it 'post x=7 y= should return -1' do
    post '/chop', {'x':'7', 'y':''}
    expect(last_response).to be_ok
    expect(last_response.body).to include 'chop:x=7,y=, resultado = -1'
  end

  it 'post x=1 y=1 should return 0' do
    post '/chop', {'x':'1', 'y':'1'}
    expect(last_response).to be_ok
    expect(last_response.body).to include 'chop:x=1,y=1, resultado = 0'
  end

  it 'post x=1 y=1,2 should return 0' do
    post '/chop', {'x':'1', 'y':'1,2'}
    expect(last_response).to be_ok
    expect(last_response.body).to include 'chop:x=1,y=1,2, resultado = 0'
  end

  it 'post x=2 y=1,2 should return 1' do
    post '/chop', {'x':'2', 'y':'1,2'}
    expect(last_response).to be_ok
    expect(last_response.body).to include 'chop:x=2,y=1,2, resultado = 1'
  end

  it 'post x=7 y=0,7,3 should return 1' do
    post '/chop', {'x':'7', 'y':'0,7,3'}
    expect(last_response).to be_ok
    expect(last_response.body).to include 'chop:x=7,y=0,7,3, resultado = 1'
  end

  it 'post x=3 y=0,7,3 should return 1' do
    post '/chop', {'x':'3', 'y':'0,7,3'}
    expect(last_response).to be_ok
    expect(last_response.body).to include 'chop:x=3,y=0,7,3, resultado = 2'
  end

  it 'post x=5 y=0,7,3 should return -1' do
    post '/chop', {'x':'5', 'y':'0,7,3'}
    expect(last_response).to be_ok
    expect(last_response.body).to include 'chop:x=5,y=0,7,3, resultado = -1'
  end

  it 'post x=3 y=0,3,7,3 should return 1' do
    post '/chop', {'x':'3', 'y':'0,3,7,3'}
    expect(last_response).to be_ok
    expect(last_response.body).to include 'chop:x=3,y=0,3,7,3, resultado = 1'
  end

end
